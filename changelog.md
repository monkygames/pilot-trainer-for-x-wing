# Release v0.1.2 - 2017-05-03
# Release v0.1.1 - 2017-05-03
## Summary
Changed fill on straight's to white

### Changed
  - Changed the fill color to white on straight's to remain consistant with banks.

## Summary
Added banks

### Changed
  - Added bank maneuvors.
  - Added arrows and numbers to movement templates.

# Release v0.1.0 - 2017-03-20
## Summary
Initial Release
