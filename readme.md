# Pilot Trainer For X-Wing

This game was initially developed for the [2017 Linux Game Jam](https://itch.io/jam/linux-jam-2017).  It only have a few days of development effort so is in a very bare bones state right now.

## About

X-Wing Miniatures is a tactical table top game that requires skill. Pilot Trainer for X-Wing sharpens pilots skills through a unique set of mini-games.


# Release
The game is currently released through itch.io.  [Link](https://monkygames.itch.io/pilot-trainer-for-x-wing)

# Development
The game uses Java & JavaFX.  The build system is gradle.

## Compiling
```
gradle build
```
## Code

### Java Code
The java code is in src/main/java/com/monkygames/ptfxw package.  It contains the following sub packages.

#### controller
The controller package holds the JavaFX controllers.  JavaFX controllers manage the interactions between the JavaFX UI Elements and java code.

#### engine
Much of the game logic is defined in this package.
I initially was using the canvas to draw on, but later found that using nodes and the scene graph was a better approach (for doing translations, rotations, eventually zooming, etc).  So GameLoop is sort of a hold over.  GameLoop right now serves as just a means of moving the timer.

##### hud
Contains objects that are used for rending to the hud.

#### game
game contains Game Objects that can be rendered and interacted with.

* Base - the primary building block for all game objects.
* BaseZone - an invisible rectangle with bounding box used to determine if the player selected the right location
* SmallBaseZone - a base zone sized for a small ship base
* ShipBase - The base class for ships.  A ship is used both as the starting zone and for placing a ship.
* SmallBase - The size of a small ship base.
* Score - Holds the players score.  It can also be used in a TableView.

##### game.maneuvers
maneuvers are game objects that are desigend to mimic the maneuvers from the table top game.

* Maneuver - The base class for all maneuvers.  Used to calculate the size and position of the maneuver.  Also can generate an object for rendering.
* ManeuverType - An enum for the types of maneuvers.
* Straight - The straight type of maneuvers which covers 1 - 5 speeds.


#### util
Contains utilities to help other classes.  WindowUtil has methods for accessing resources for instance.

## Creating a Release
Since I didn't have much time, I manually created a build.  I tried to integrate the itch.io butler but couldn't get that working.  For now, it will still be a manual process.  If more work goes into this, butler could be used OR Bitrock Install builder (will need to apply for an open source license).


# Licensing
I am releasing this under the GPLv3.  I will happy accept PRs!