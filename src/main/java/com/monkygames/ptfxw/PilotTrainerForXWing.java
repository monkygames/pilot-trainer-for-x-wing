/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monkygames.ptfxw;

import com.monkygames.ptfxw.controller.StartController;
import com.monkygames.ptfxw.util.WindowUtil;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author spethm
 */
public class PilotTrainerForXWing extends Application {

    public static final String VERSION = "0.1.2";

    private StartController controller;

    @Override
    public void start(Stage primaryStage) throws Exception {
        // initialize the start ui
	WindowUtil.setApplication(this);
	controller = (StartController)WindowUtil.loadScene("menu/Start.fxml");
	controller.showStage();
    }
    
}
