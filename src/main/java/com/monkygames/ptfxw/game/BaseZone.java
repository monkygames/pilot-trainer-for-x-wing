package com.monkygames.ptfxw.game;

import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * A base zone to count for scoring.
 */
public class BaseZone extends Base{

    private final Rectangle base;
    private static final int X_OFF = 12;
    private static final int Y_OFF = 16;

    public BaseZone(int width, int height) {
	super(width+X_OFF, height+Y_OFF);
	base = new Rectangle(0, 0, bounds.getWidth(), bounds.getHeight());
	//base.setStroke(Color.ROYALBLUE);
	//base.setFill(Color.ROYALBLUE);
	//base.setVisible(false);
    Color invis = Color.rgb(0,0,0,0);
    base.setStroke(invis);
    base.setFill(invis);

	group.getChildren().add(base);
    }

    public void enableView(){
	base.setStroke(Color.ROYALBLUE);
	base.setFill(Color.ROYALBLUE);
	base.setVisible(true);
    }
    
    @Override
    public void setPosition(double x, double y){
	super.setPosition(x - X_OFF/2, y - Y_OFF/2 );
    }

    /**
     * Used to check landing zone
     * @param bounds The bounds to apply the bounding box.
     * @return 
     */
    public BoundingBox getBoundingBox(Bounds bounds){
	return new BoundingBox(bounds.getMinX(),bounds.getMinY(),width,height) ;
    }
}
