package com.monkygames.ptfxw.game;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 */
public class Score {

    private final SimpleStringProperty pilotName;
    private final SimpleStringProperty ship;
    private final SimpleStringProperty type;
    private final SimpleIntegerProperty score;

    public Score(String pilotName, String ship, String type, int score){
	this.pilotName = new SimpleStringProperty(pilotName);
	this.ship      = new SimpleStringProperty(ship);
	this.type      = new SimpleStringProperty(type);
	this.score     = new SimpleIntegerProperty(score);
    }

    public String getPilotName() {
	return pilotName.get();
    }

    public String getShip() {
	return ship.get();
    }

    public String getType() {
	return type.get();
    }

    public Integer getScore() {
	return score.get();
    }

    
}
