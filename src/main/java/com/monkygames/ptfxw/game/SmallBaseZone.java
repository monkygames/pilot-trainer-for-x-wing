/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monkygames.ptfxw.game;

/**
 * A base zone for the small bases.
 */
public class SmallBaseZone extends BaseZone{

    public SmallBaseZone() {
	super(SmallBase.BASE_WIDTH, SmallBase.BASE_HEIGHT);
    }
}
