/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monkygames.ptfxw.game;

// The small bases are exactly 40mm x 40mm.
// The nubs are 3mm wide and centered at 7.5mm and 31.5mm from the edge.
// The hole in a card for the circular peg is 12mm in diameter.
// The cards are 34mm x 40mm. The peg itself is 4mm close to the base.

public class SmallBase extends ShipBase{
    public static final int BASE_WIDTH    = 34;
    public static final int BASE_HEIGHT   = 40;
    public static final int NUB_WIDTH     = 3;
    public static final int NUB_HEIGHT    = 3;
    public static final int MID_RADIUS    = 12;

    public SmallBase(int canvas_width, int canvas_height){
	super(canvas_width,canvas_height,BASE_WIDTH,BASE_HEIGHT,NUB_WIDTH,NUB_HEIGHT,MID_RADIUS);
    }

}
