package com.monkygames.ptfxw.game;

import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.transform.Rotate;

public class Base {

    protected Rectangle2D bounds;
    protected Group group;
    protected double pivote_x;
    protected double pivote_y;
    private final Rotate rotate;
    protected double width, height;

    public Base(double width, double height){
	this.width = width;
	this.height = height;
	bounds   = new Rectangle2D(0,0,width,height);
	group    = new Group();
	pivote_x = bounds.getWidth()/2;
	pivote_y = bounds.getHeight()/2;
	rotate = new Rotate(0,pivote_x,pivote_y);
	group.getTransforms().add(rotate);
    }

    public Rectangle2D getBoundary() {
	return bounds;
    }

    public boolean intersects(Base s) {
	return s.getBoundary().intersects(this.getBoundary());
    }

    /**
     * Set the position based on the middle of the base.
     * @param x the x-axis.
     * @param y the y-axis.
     */
    public void setMidPosition(double x, double y){
	setPosition(x - bounds.getWidth()/2, y - bounds.getHeight()/2);
    }

    public void setPosition(double x, double y){
	bounds = new Rectangle2D(x,y,bounds.getWidth(),bounds.getHeight());
	group.setLayoutX(x);
	group.setLayoutY(y);
    }

    public Group getGroup(){
	return group;
    }

    public double getWidth() {
	return width;
    }

    public double getHeight() {
	return height;
    }

    public void rotate(double rot){
	rotate.setAngle(rot);
	rotate.setAxis(Rotate.Z_AXIS);
    }

    public void rotate(double rot, double pivot_x, double pivot_y){
	rotate.setPivotX(pivot_x);
	rotate.setPivotY(pivot_y);
	rotate.setAxis(Rotate.Z_AXIS);
	rotate.setAngle(rot);
    }
}
