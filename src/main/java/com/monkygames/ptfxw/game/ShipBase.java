/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monkygames.ptfxw.game;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

// The small bases are exactly 40mm x 40mm.
// The nubs are 3mm wide and centered at 7.5mm and 31.5mm from the edge.
// The hole in a card for the circular peg is 12mm in diameter.
// The cards are 34mm x 40mm. The peg itself is 4mm close to the base.

public class ShipBase extends Base{
    private Rectangle base;
    private Rectangle nub1, nub2, nub3, nub4;
    private Circle mid;

    private final double start_x;
    private final double start_y;

    protected double nub_width;
    protected double nub_height;
    protected double base_width, base_height;
    protected double nub_x1, nub_x2;
    protected double nub_x1_off, nub_x2_off;
    protected double mid_radius;

    public ShipBase(int canvas_width, int canvas_height, int base_width, int base_height, int nub_width, int nub_height, int mid_radius){
	super(base_width, base_height);

	this.base_width = base_width;
	this.base_height = base_height;

	this.nub_width  = nub_width;
	this.nub_height = nub_height;

	this.mid_radius = mid_radius;

	start_x = canvas_width/2.0 - base_width/2.0;
	start_y = canvas_height-base_height-nub_height;

	nub_x1 = 7.5;
	nub_x2 = base_width -7.5;
	nub_x1_off = nub_x1 - nub_width/2;
	nub_x2_off = nub_x2 - nub_width/2;

	setup(0,0);
    }

    /**
     * Sets up the components that make up the small ship base
     **/
    public void setup(double x, double y){

	base = new Rectangle(x,y,base_width,base_height);
	base.setStroke(Color.BLACK);
	base.setFill(null);
	base.setStrokeWidth(2);

	nub1 = new Rectangle(x+nub_x1_off,y-nub_height,nub_width,nub_height);
	nub2 = new Rectangle(x+nub_x2_off,y-nub_height,nub_width,nub_height);
	nub3 = new Rectangle(x+nub_x1_off,y+base_height,nub_width,nub_height);
	nub4 = new Rectangle(x+nub_x2_off,y+base_height,nub_width,nub_height);

	// create the middle
	mid = new Circle(x + base_width/2, y + base_height/2, mid_radius/2);
	mid.setStroke(Color.BLACK);
	mid.setFill(null);
	mid.setStrokeWidth(2);

	group.getChildren().addAll(base,nub1,nub2,nub3,nub4,mid);
    }

    public void setStartingPosition(){
	setPosition(start_x,start_y);
    }

    public double getNubWidth(){
	return nub_width;
    }

    public double getNubX1(){
	return nub_x1;
    }
    public double getNubX2(){
	return nub_x2;
    }
}
