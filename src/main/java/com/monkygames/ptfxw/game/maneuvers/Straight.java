package com.monkygames.ptfxw.game.maneuvers;

import com.monkygames.ptfxw.game.ShipBase;
import com.monkygames.ptfxw.util.WindowUtil;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

/**
 * Straight Maneuvers.
 */
public class Straight extends Maneuver{

    public static final double SPEED_SIZE = 40;


    /**
     * Creates a new straight maneuver.
     * @param speed 
     */
    public Straight(int speed){
	super(speed,5,ManeuverType.STRAIGHT,ManeuverDirection.STRAIGHT);
    }

    @Override
    public Group getTemplate(ShipBase shipBase) {
	Group group = new Group();

	double x = shipBase.getBoundary().getMinX() + shipBase.getNubX1() + shipBase.getNubWidth()/2;
	double y = shipBase.getBoundary().getMinY()- SPEED_SIZE*speed;
	double w = shipBase.getNubX2() - shipBase.getNubX1() - shipBase.getNubWidth();
	double h = SPEED_SIZE*speed;

	Rectangle rectangle = new Rectangle(x,y,w,h);
	rectangle.setStroke(Color.BLACK);
	rectangle.setFill(Color.WHITE);
	rectangle.setStrokeWidth(1);
	group.getChildren().add(rectangle);

	// add the speed and direction
	Label speedL = new Label(speed+"");
	speedL.setLayoutX(x+4);
	speedL.setLayoutY(y+h-18);
	group.getChildren().add(speedL);

	// add direction
	Font xwingFont = WindowUtil.loadFont("xwing-miniatures.ttf", 12);
	Label dirL = new Label("8");
	dirL.setFont(xwingFont);
	dirL.setLayoutX(x+4);
	dirL.setLayoutY(y+h-32);
	group.getChildren().add(dirL);

	return group;
    }

    @Override
    public void rotateShip(ShipBase shipBase) {
	// do nothing
    }
}
