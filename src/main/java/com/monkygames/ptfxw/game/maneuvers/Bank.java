package com.monkygames.ptfxw.game.maneuvers;

import com.monkygames.ptfxw.game.BaseZone;
import com.monkygames.ptfxw.game.ShipBase;
import com.monkygames.ptfxw.util.WindowUtil;
import java.security.SecureRandom;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;

/**
 * Bank Maneuvers.
 */
public class Bank extends Maneuver{

//size in mm
//: 40.0, 80.0, 120.0, 160.0, 200.0

// 90' secter
//Turn inside radii in mm
//    : 25.0, 53.0, 80.0
//Turn outside radii in mm
//    : 45.0, 73.0, 100.0

// 45' sectert
//Bank inside radii in mm
//    : 70.0, 120.0, 170.0
//Bank outside radii in mm
//    : 90.0, 140.0, 190.0

    public static final double SPEED_SIZE = 40;

    //public static final double[] RADIUS_IN = {70,120,170};
    //public static final double[] RADIUS_OUT = {90,140,190};
    private static final double ANGLE       = 45.0;
    public static final double[] RADIUS_IN  = {57,100,127};
    public static final double[] RADIUS_OUT = {72,116,144};

    public static final String BANK_LEFT  = "7";
    public static final String BANK_RIGHT = "9";

    /**
     * Creates a new bank maneuver.
     * @param speed 
     */
    public Bank(int speed){
	super(speed,3,ManeuverType.BANK,ManeuverDirection.LEFT);
    }

    @Override
    public Group getTemplate(ShipBase shipBase) {
	double x = shipBase.getBoundary().getMinX() + shipBase.getNubX1() + shipBase.getNubWidth()/2;
	double y = shipBase.getBoundary().getMinY();
	double w = shipBase.getNubX2() - shipBase.getNubX1() - shipBase.getNubWidth();
	double h = SPEED_SIZE*speed;

	double x1 = x;
	double y1 = y;

	double x2 = x+w;
	double y2 = y1;

	double radius1 = RADIUS_IN[speed -1];
	double radius2 = RADIUS_OUT[speed -1];

	// check for banking left or right
	if(direction == ManeuverDirection.LEFT){
	    return createTemplate(x1,y1,x2,y2,radius1,radius2,ANGLE,BANK_LEFT);
	}
	// bank right
	//return createTemplate(x2,y2,x1,y1,radius2,radius1,ANGLE,BANK_RIGHT);
	return createTemplate(x1,y1,x2,y2,radius2,radius1,ANGLE,BANK_RIGHT);
    }

    private Group createTemplate(double x1, double y1, double x2, double y2,double radius1, double radius2, double angle, String bankText){
	Group group = new Group();
        Arc arc_in = createArc(radius1,x1,y1,angle);
        Arc arc_out = createArc(radius2,x2,y2,angle);
	group.getChildren().add(arc_in);
	group.getChildren().add(arc_out);
	double speedX,speedY;
	double dirX,dirY;

	Point2D point_in,point_out;
	if(direction == ManeuverDirection.LEFT){
	    point_in  = this.getArcEnd(radius1, x1, y1,angle);
            point_out = this.getArcEnd(radius2, x2, y2,angle);
	    speedX    = x1+4;
	    speedY    = y1-18;
	    dirX      = x1;
	    dirY      = y1-32;
	}else{
            point_in  = this.getArcEnd(radius2, x2, y2,angle-180);
	    point_out = this.getArcEnd(radius1, x1, y1,angle-180);
	    speedX    = x1+4;
	    speedY    = y1-18;
	    dirX      = x1+8;
	    dirY      = y1-32;
	}

	Line line = new Line();
	line.setStartX(point_in.getX());
	line.setStartY(point_in.getY());
	line.setEndX(point_out.getX());
	line.setEndY(point_out.getY());
	line.setStroke(Color.BLACK);
	line.setStrokeWidth(1);
	group.getChildren().add(line);

	// add the speed and direction
	Label speedL = new Label(speed+"");
	speedL.setLayoutX(speedX);
	speedL.setLayoutY(speedY);
	group.getChildren().add(speedL);

	// add direction
	Font xwingFont = WindowUtil.loadFont("xwing-miniatures.ttf", 12);
	Label dirL = new Label(bankText);
	dirL.setFont(xwingFont);
	dirL.setLayoutX(dirX);
	dirL.setLayoutY(dirY);
	group.getChildren().add(dirL);

	return group;
    }

    private Arc createArc(double radius,double x, double y, double angle){
	// defaults for bank left
        double xc         = x - radius*Math.cos(0);
	double yc         = y - radius*Math.sin(0);
	double startAngle = 0;
	double endAngle   = angle;

	if(direction == ManeuverDirection.RIGHT){
          xc         = x + radius*Math.cos(0);
	  startAngle = 180;
	  endAngle   = -45;
	}

	Arc arc = new Arc();
	arc.setRadiusX(radius);
	arc.setRadiusY(radius);
	arc.setStartAngle(startAngle);
	arc.setLength(endAngle);
	arc.setType(ArcType.OPEN);
	arc.setStroke(Color.BLACK);
	arc.setFill(null);
	arc.setStrokeWidth(1);
	arc.setCenterX(xc);
	arc.setCenterY(yc);
	return arc;
    }

    private Point2D getArcEnd(double radius, double x, double y, double angle){
        double xc = x - radius*Math.cos(0);
	double yc = y - radius*Math.sin(0);
	double useAngle = -1*angle;

	if(direction == ManeuverDirection.RIGHT){
          xc = x + radius*Math.cos(0);
	  useAngle = angle;
	}

	double x2 = radius*Math.cos(Math.toRadians(useAngle)) + xc;
	double y2 = radius*Math.sin(Math.toRadians(useAngle)) + yc;
	return new Point2D(x2,y2);
    }

    @Override
    public BaseZone nextMove(ShipBase base){
	// set the speed
	setSpeed(random.nextInt(maxMoves)+1);

	// set the direction
	if(random.nextInt(2) == 0){
	    direction = ManeuverDirection.LEFT;
	}else{
	    direction = ManeuverDirection.RIGHT;
	}

	double x = base.getBoundary().getMinX() + base.getNubX1() + base.getNubWidth()/2;
	double y = base.getBoundary().getMinY();
	double w = base.getNubX2() - base.getNubX1() - base.getNubWidth();

	double x1 = x;
	double y1 = y;

	double x2 = x+w;
	double y2 = y1;

	double radius1 = RADIUS_IN[speed -1];
	double radius2 = RADIUS_OUT[speed - 1];
	double rotate  = -45;

	Point2D point_in; 
	Point2D point_out;
	if(direction == ManeuverDirection.LEFT){
	    point_in  = this.getArcEnd(radius1, x1, y1,ANGLE);
            point_out = this.getArcEnd(radius2, x2, y2,ANGLE);
	}else{
            point_in  = this.getArcEnd(radius2, x2, y2,ANGLE-180);
	    point_out = this.getArcEnd(radius1, x1, y1,ANGLE-180);
	    rotate    = 45;
	}

	// create the base
	BaseZone baseZone = new BaseZone((int)base.getBoundary().getWidth(),(int)base.getBoundary().getHeight());

	// calculate slope
	double slope = (point_out.getY() - point_in.getY())/(point_out.getX() - point_in.getX());

	// calculate the corner
	double x_bot = point_in.getX() - base.getNubWidth() / Math.sqrt(1+Math.pow(slope, 2));
	double y_bot = slope*(x_bot - point_in.getX())+ point_in.getY();
	y_bot -= base.getHeight();

	// need to broaden zone.
	if(direction == ManeuverDirection.LEFT){
	    x_bot -= 24;
	    y_bot -= 6;
	}else{
	    x_bot -= 10;
	    y_bot += 8;
	}

	baseZone.rotate(rotate);
	baseZone.setPosition(x_bot,y_bot);

	//baseZone.enableView();
	return baseZone;
    }

    @Override
    public void rotateShip(ShipBase shipBase){
	if(direction == ManeuverDirection.LEFT){
	    shipBase.rotate(-45);
	}else{
	    shipBase.rotate(45);
	}
    }
}
