package com.monkygames.ptfxw.game.maneuvers;

import com.monkygames.ptfxw.game.Base;
import com.monkygames.ptfxw.game.BaseZone;
import com.monkygames.ptfxw.game.ShipBase;
import java.util.Random;
import javafx.scene.Group;

/**
 * A maneuver that modifies the position of a base.
 */
public abstract class Maneuver {

    protected int speed;
    protected Random random;
    protected int maxMoves;
    protected ManeuverType type;
    protected ManeuverDirection direction;

    public Maneuver(int speed, int maxMoves, ManeuverType type, ManeuverDirection direction){
	this.speed     = speed;
	this.maxMoves  = maxMoves;
	this.type      = type;
	this.direction = direction;
	random         = new Random();
    }

    public int getSpeed(){
	return speed;
    }

    public void setSpeed(int speed){
	this.speed = speed;
    }

    public ManeuverType getType(){
	return type;
    }

    public ManeuverDirection getDirection(){
	return direction;
    }
    public void setManeuverDirection(ManeuverDirection direction){
	this.direction = direction;
    }

    public void moveBase(Base base, double x, double y){
	//double x = base.getGroup().getLayoutX();

	double width = base.getBoundary().getWidth();
	double height = base.getBoundary().getHeight();

	// HACK, need to get the right speed size depending on type of maneuver.
	double calc_y = y - Straight.SPEED_SIZE*(speed+1);

	base.getGroup().setLayoutX(x);
	base.getGroup().setLayoutY(calc_y);
        base.setPosition(x, calc_y);
    }

    public abstract Group getTemplate(ShipBase shipBase);

    /**
     * If the shipbase should be rotated, rotate it, otherwise, do nothing.
     * @param shipBase the shipbase to rotate.
     */
    public abstract void rotateShip(ShipBase shipBase);

    /**
     * Sets up the next move.
     * @param base the starting position of the maneuver.
     * @return the base zone where the user has to click to.
     */
    public BaseZone nextMove(ShipBase base){
	// set the speed
	setSpeed(random.nextInt(maxMoves)+1);

	double x = base.getBoundary().getMinX();
	double y = base.getBoundary().getMinY();

	// create the base
	BaseZone baseZone = new BaseZone((int)base.getBoundary().getWidth(),(int)base.getBoundary().getHeight());

	baseZone.setPosition(x, y);

	moveBase(baseZone,x,y);
	//baseZone.enableView();
	return baseZone;
    }
}
