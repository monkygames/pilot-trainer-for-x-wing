/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monkygames.ptfxw.controller;

import com.monkygames.ptfxw.game.Score;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author spethm
 */
public class ScoreBoardController extends FXMLController implements Initializable {

    @FXML
    TableColumn pilotCol;

    @FXML
    TableColumn shipCol;

    @FXML
    TableColumn typeCol;
    @FXML
    TableColumn scoreCol;

    @FXML
    TableView scoreTable;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
	// TODO
	//pilotCol.setCellValueFactory(new PropertyValueFactory<Score, String>("pilotName"));
	//shipCol.setCellValueFactory(new PropertyValueFactory<Score, String>("ship"));
	//typeCol.setCellValueFactory(new PropertyValueFactory<Score, String>("type"));
	//scoreCol.setCellValueFactory(new PropertyValueFactory<Score, Integer>("score"));
	pilotCol.setCellValueFactory(new PropertyValueFactory<>("pilotName"));
	shipCol.setCellValueFactory(new PropertyValueFactory<>("ship"));
	typeCol.setCellValueFactory(new PropertyValueFactory<>("type"));
	scoreCol.setCellValueFactory(new PropertyValueFactory<>("score"));
    }    


    @FXML
    public void okEventFired(){
	backController.showStage();
	stage.hide();
    }
    
    public TableView getTable(){
	return scoreTable;
    }
}
