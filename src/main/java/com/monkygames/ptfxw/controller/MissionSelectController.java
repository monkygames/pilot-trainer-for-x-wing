/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monkygames.ptfxw.controller;

import com.monkygames.ptfxw.game.Score;
import com.monkygames.ptfxw.game.maneuvers.ManeuverType;
import com.monkygames.ptfxw.util.WindowUtil;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;

/**
 * FXML Controller class
 *
 * @author spethm
 */
public class MissionSelectController extends FXMLController implements Initializable {

    @FXML
    private ToggleButton straightTB;
    @FXML
    private ToggleButton bankTB;
    @FXML
    private ToggleButton turnTB;
    @FXML
    private ToggleButton tutorialTB;
    @FXML
    private ToggleButton specialTB;
    @FXML
    private ToggleButton randomTB;

    private ToggleGroup group;

    private GameController gameController;

    private String pilotName;
    /**
     * Contains the scores.
     */
    private ObservableList<Score> scoresOL;
    private ArrayList<Score> scores;
    private TableView table;

    private TutorialController tutorialController;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
	// TODO
	group = new ToggleGroup();
	straightTB.setToggleGroup(group);
	bankTB.setToggleGroup(group);
	turnTB.setToggleGroup(group);
	tutorialTB.setToggleGroup(group);
	specialTB.setToggleGroup(group);
	randomTB.setToggleGroup(group);

	gameController = new GameController();
	gameController.setMissionController(this);
        gameController.setBackController(this);
	scores = new ArrayList<>();

	tutorialController = (TutorialController)WindowUtil.loadScene("menu/Tutorial.fxml");
	tutorialController.setBackController(this);
    }
    
    @FXML
    public void backEventFired(){
	backController.showStage();
	stage.hide();
    }

    public void setPilotName(String pilotName){
	this.pilotName = pilotName;
    }

    public void setTable(TableView table){
	this.table = table;

    }

    @FXML
    public void playEventFired(){
	ToggleButton tb = (ToggleButton)group.getSelectedToggle();
	if(tb == straightTB){
	    gameController.setupGame(ManeuverType.STRAIGHT);
	    gameController.showStage();
	    this.hideStage();
	}else if(tb == bankTB){
	    gameController.setupGame(ManeuverType.BANK);
	    gameController.showStage();
	    this.hideStage();
	}else if(tb == turnTB){
	}else if(tb == tutorialTB){
	    tutorialController.showStage();
	    this.hideStage();
	}else if(tb == specialTB){
	}else if(tb == randomTB){
	}
    }

    /**
     * Sets the results
     * @param score  
     */
    public void setResults(int score){
	scores.add(new Score(pilotName,"Small","Straight",score));
	// update the table based on score
	table.setItems(FXCollections.observableArrayList(scores));
    }
}
