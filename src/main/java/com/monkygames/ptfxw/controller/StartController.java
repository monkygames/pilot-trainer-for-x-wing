/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monkygames.ptfxw.controller;

import com.monkygames.ptfxw.PilotTrainerForXWing;
import com.monkygames.ptfxw.util.WindowUtil;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author spethm
 */
public class StartController extends FXMLController implements Initializable {

    private ScoreBoardController scoreBoardController;
    private MissionSelectController missionController;
    @FXML
    private TextField player_field;

    @FXML
    private AnchorPane popup;


    @FXML
    private Button playB;

    @FXML
    private Button scoreboardB;

    @FXML
    private Label versionL;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
	// TODO
	scoreBoardController = (ScoreBoardController)WindowUtil.loadScene("menu/ScoreBoard.fxml");
	scoreBoardController.setBackController(this);
	missionController = (MissionSelectController)WindowUtil.loadScene("menu/MissionSelect.fxml");
	missionController.setBackController(this);
	missionController.setTable(scoreBoardController.getTable());
	versionL.setText("Version: "+PilotTrainerForXWing.VERSION);
    }    

    @FXML
    public void scoreboardEventFired(){
	scoreBoardController.showStage();
	this.hideStage();
    }

    @FXML
    public void missionEventFired(){
	// check if a name has been input
	String text = player_field.getText();

	if(text == null || text.equals("")){
	    popup.setVisible(true);
	    // disable
	    playB.setDisable(true);
	    scoreboardB.setDisable(true);
	}else{
	    missionController.setPilotName(text);
	    missionController.showStage();
	    this.hideStage();
	}
    }

    @FXML
    public void okEventFired(){
	popup.setVisible(false);
	playB.setDisable(false);
	scoreboardB.setDisable(false);
    }
    
}
