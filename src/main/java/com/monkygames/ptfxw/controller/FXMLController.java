/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monkygames.ptfxw.controller;

import javafx.stage.Stage;

/**
 *
 */
public class FXMLController {
    protected Stage stage;
    protected FXMLController backController;

    public void setStage(Stage stage){
	this.stage = stage;
    }

    public void showStage(){
	stage.show();
    }

    public void hideStage(){
	stage.hide();
    }

    public void setBackController(FXMLController backController){
	this.backController = backController;
    }
    
}
