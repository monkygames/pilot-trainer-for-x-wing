/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monkygames.ptfxw.controller;

import com.monkygames.ptfxw.util.WindowUtil;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author spethm
 */
public class TutorialController extends FXMLController implements Initializable {

    @FXML
    ImageView imageV;

    private ArrayList<Image> images;

    private int pos = 0;

    private MissionSelectController missionController;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

	// load the images
	images = new ArrayList<>();
	String id;
	for(int i = 1; i < 14; i++){
	    if(i < 10){
		id = "00"+i;
	    }else{
		id = "0"+i;
	    }

            images.add(WindowUtil.getImage("tutorial/p"+id+".png"));

	    imageV.setImage(images.get(0));
	    pos = 0;
	}
    }    

    private void setPage(int page){
	imageV.setImage(images.get(page));
    }

    @FXML
    public void backEventFired(){
	if(pos == 0){
	    // exit
	    backController.showStage();
	    stage.hide();
	}else{
	    pos--;
	    setPage(pos);
	}

    }

    @FXML void nextEventFired(){
	pos++;
	if(pos >= images.size()){
	    // exit
	    backController.showStage();
	    stage.hide();
	}else{
	    setPage(pos);
	}

    }
}
