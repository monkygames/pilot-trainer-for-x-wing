package com.monkygames.ptfxw.controller;

import com.monkygames.ptfxw.engine.GameManager;
import com.monkygames.ptfxw.game.maneuvers.ManeuverType;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 */
public class GameController extends FXMLController implements Initializable {

    public static final int CANVAS_WIDTH  = 914/2;
    public static final int CANVAS_HEIGHT = 914/2;

    private MissionSelectController missionController;
    private GameManager gameManager;

    public GameController(){
	this.stage = new Stage();
	stage.setTitle("Pilot Training for X-Wing");
	gameManager = new GameManager(stage,this);
    }

    public void setupGame(ManeuverType type){
	gameManager.setupGame(type, GameManager.ShipType.SMALL);
    }

    @Override
    public void showStage(){
	super.showStage();
	gameManager.start();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }    
    
    @FXML
    public void backEventFired(){
	backController.showStage();
	stage.hide();
    }
    public void setResults(int score){
	missionController.setResults(score);
    }

    public void setMissionController(MissionSelectController missionController){
	this.missionController = missionController;
    }

    public void okEventFired(){

    }

}
