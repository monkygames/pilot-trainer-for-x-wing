package com.monkygames.ptfxw.engine;

import com.monkygames.ptfxw.controller.GameController;
import com.monkygames.ptfxw.engine.hud.HudBot;
import com.monkygames.ptfxw.engine.hud.ManeuverSlot;
import com.monkygames.ptfxw.engine.hud.PointsSlot;
import com.monkygames.ptfxw.game.BaseZone;
import com.monkygames.ptfxw.game.ShipBase;
import com.monkygames.ptfxw.game.SmallBase;
import com.monkygames.ptfxw.game.maneuvers.Bank;
import com.monkygames.ptfxw.game.maneuvers.Maneuver;
import com.monkygames.ptfxw.game.maneuvers.ManeuverType;
import com.monkygames.ptfxw.game.maneuvers.Straight;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

// TODO
// * Increase the size of the canvas
// * Add zooming with the mouse wheel

/**
 * Handles managing all game components.
 */
public class GameManager {
    private final GameLoop gameLoop;
    public static final int CANVAS_WIDTH  = 914/2;
    public static final int CANVAS_HEIGHT = 914/2;
    //public static final int CANVAS_WIDTH  = 914;
    //public static final int CANVAS_HEIGHT = 914;

    // components
    /**
     * The starting ship base (center bot).
     */
    private ShipBase startShipBase;
    /**
     * The ZONE that will check if it contains the ship base.
     */
    private BaseZone baseZone;
    /**
     * The maneuver that is being played.
     */
    private Maneuver maneuver;

    // groups
    private Group templateGroup;
    private Group shipBaseGroup;
    private Group startBaseGroup;
    private Group zoneGroup;
    private Group hudGroup;
    private Group zoomGroup;

    /**
     * The template that has been used
     */
    private Group usedTemplate;

    // hud components
    private Timer timer;
    private HudBot hudBot;
    private ManeuverSlot maneuverSlot;
    private PointsSlot pointsSlot;
    private Dice dice;

    private static final double MID_X = CANVAS_WIDTH/2.0;
    private static final double MID_Y = CANVAS_WIDTH/2.0;
    private final GameController gameController;

    public enum ShipType {
	SMALL, LARGE
    }


    public GameManager(Stage stage, GameController gameController){
	this.gameController = gameController;

	// to put stackable components 
	StackPane stack = new StackPane();

	// setup groups
	Group root    = new Group();

	Scene scene   = new Scene(root);
	Canvas canvas = new Canvas(CANVAS_WIDTH,CANVAS_HEIGHT);

	// setup game loop and mouse handling 
	GraphicsContext gc = canvas.getGraphicsContext2D();
	gameLoop           = new GameLoop(gc,CANVAS_WIDTH,CANVAS_HEIGHT);
	gameLoop.setGameManager(this);
	scene.setOnMouseClicked(gameLoop);

	setupGroups();
	setupHUD();

	// pack components into scene
	zoomGroup.getChildren().addAll(startBaseGroup,zoneGroup,templateGroup,shipBaseGroup);
	Pane pane = new Pane();
	pane.setPrefSize(CANVAS_WIDTH,CANVAS_HEIGHT);

	/*
	pane.setOnScroll(new EventHandler<ScrollEvent>() {
	    @Override
	    public void handle(ScrollEvent event) {
		event.consume();

		if (event.getDeltaY() == 0) {
		    return;
		}

		double scaleFactor
			= (event.getDeltaY() > 0)
				? SCALE_DELTA
				: 1 / SCALE_DELTA;

		zoomGroup.setScaleX(zoomGroup.getScaleX() * scaleFactor);
		zoomGroup.setScaleY(zoomGroup.getScaleY() * scaleFactor);
	    }
	});
	*/

	pane.layoutBoundsProperty().addListener(new ChangeListener<Bounds>() {
	    @Override
	    public void changed(ObservableValue<? extends Bounds> observable, Bounds oldBounds, Bounds bounds) {
		pane.setClip(new Rectangle(bounds.getMinX(), bounds.getMinY(), bounds.getWidth(), bounds.getHeight()));
	    }
	});

	//pane.getChildren().addAll(startBaseGroup, zoneGroup,templateGroup,shipBaseGroup,hudGroup);
	pane.getChildren().addAll(zoomGroup,hudGroup);
	stack.getChildren().addAll(canvas,pane,hudBot.getBorderPane());
	root.getChildren().addAll(stack);
	stage.setScene(scene);
    }

    public void start(){
	gameLoop.start();

    }

    private void setupHUD(){
	// setup maneuver slot
	maneuverSlot = new ManeuverSlot(CANVAS_WIDTH,CANVAS_HEIGHT);
	gameLoop.setManeuverSlot(maneuverSlot);
	hudGroup.getChildren().add(maneuverSlot.getGroup());

	// setup points slot
	pointsSlot = new PointsSlot(CANVAS_WIDTH,CANVAS_HEIGHT);
	gameLoop.setPointsSlot(pointsSlot);
	hudGroup.getChildren().add(pointsSlot.getGroup());

	// setup timer
	timer = new Timer(CANVAS_WIDTH,CANVAS_HEIGHT,this);
	gameLoop.setTimer(timer);
	hudGroup.getChildren().add(timer.getGroup());

	dice = new Dice(CANVAS_WIDTH,CANVAS_HEIGHT);
	hudGroup.getChildren().add(dice.getGroup());

	// setup bottom hud pieces
	hudBot = new HudBot(CANVAS_WIDTH,CANVAS_HEIGHT,this);
    }

    final double SCALE_DELTA = 1.1;
    private void setupGroups(){
        templateGroup  = new Group();
        shipBaseGroup  = new Group();
	startBaseGroup = new Group();
	zoneGroup      = new Group();
	hudGroup       = new Group();
	zoomGroup      = new Group();
	gameLoop.setTemplateGroup(templateGroup);
	gameLoop.setShipBaseGroup(shipBaseGroup);
    }

    /**
     * Sets the result of the ship.
     */
    public void setResult(boolean ok){

	// display the template
	usedTemplate = maneuver.getTemplate(startShipBase);
	templateGroup.getChildren().add(usedTemplate);

	// update the score
	if(ok){
	    // TODO increment score
	    pointsSlot.incrementPoints();
	    dice.setHit();
	}else{
	    dice.setMiss();
	}
    }

    /**
     * The time has runout.
     */
    public void timeExpiredEventFired(){
	gameLoop.stopTracking();
	// disable ok button
    }

    public void okEventFired(){

	// check if we are out of time
	if(timer.outOfTime()){
	    // make sure can't click
	    gameLoop.stop();
	    // go back
	    //gameController.backEventFired();
	    //moveCleanup();
	    //pointsSlot.resetPoints();
	    hudBot.disable();

	}else{
	    // load the next button
            moveCleanup();
	    this.setupMove();
	}

    }

    public void quitEventFired(){
	if(timer.outOfTime()){
	    // game has been completed, so record score
	    gameController.setResults(pointsSlot.getPoints());
	}
	// go back
	gameController.backEventFired();
	moveCleanup();
	// reset score
	pointsSlot.resetPoints();
    }

    /**
     * Sets up the game based on the type.
     * @param maneuverType
     * @param shipType
     */
    public void setupGame(ManeuverType maneuverType, ShipType shipType){

	hudBot.reset();
	gameLoop.reset();

	// populate game components

	switch(shipType){

	    case SMALL:
	        startShipBase = new SmallBase(CANVAS_WIDTH,CANVAS_HEIGHT);
	        startShipBase.setStartingPosition();
		//baseZone = new SmallBaseZone();
		// set the starting position
	        //baseZone.setMidPosition(MID_X, CANVAS_HEIGHT);
		break;
	    case LARGE:
		break;
	}

	switch(maneuverType){
	    case STRAIGHT:
		maneuver = new Straight(1);
		break;
	    case BANK:
		maneuver = new Bank(1);

		break;
	    case TURN:

		break;

	}

	// add components
	startBaseGroup.getChildren().add(startShipBase.getGroup());
	gameLoop.setShipType(shipType);
	setupMove();
    }

    /**
     * Sets the move up for the player.
     */
    public void setupMove(){
	// get maneuver
	baseZone = maneuver.nextMove(startShipBase);
	zoneGroup.getChildren().add(baseZone.getGroup());

	// disable eventually
	//baseZone.enableView();

	// add to game loop
	gameLoop.setBaseZone(baseZone);

	// update manuever slot
	maneuverSlot.setSpeed(maneuver.getSpeed());
	maneuverSlot.setManeuver(maneuver.getType());
	maneuverSlot.setDirection(maneuver.getDirection());

	// update game loop
	gameLoop.setManeuver(maneuver);
    }

    public void moveCleanup(){
	// remove baseZone
	if(baseZone != null){
	    zoneGroup.getChildren().remove(baseZone.getGroup());
	}

	// remove template base if it exists
	if(usedTemplate != null){
	    templateGroup.getChildren().remove(usedTemplate);
	}

	// remove dice
	dice.reset();

	// remove user selected ship base
	gameLoop.reset();
    }
    
}
