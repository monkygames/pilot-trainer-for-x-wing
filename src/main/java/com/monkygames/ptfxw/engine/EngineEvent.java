package com.monkygames.ptfxw.engine;

/**
 *
 */
public interface EngineEvent {

    /**
     * Notifies the interface that it should update.
     */
    public void update();
}
