/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monkygames.ptfxw.engine;

import com.monkygames.ptfxw.util.WindowUtil;
import java.util.Calendar;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author spethm
 */
public class Dice implements EngineEvent{

    private long prevTime;
    private long currentTime;
    private double calc;

    private final double pos_x;
    private final double pos_y;

    private Group group;

    private Image hit,miss;
    private Label hitL,missL;

    private boolean counting = false;

    private final int IMAGE_WIDTH = 67;
    private final int IMAGE_HEIGHT= 59;

    private static final int TIMEOUT = 1*1000;

    private Label currentL;

    public Dice(int canvas_width, int canvas_height){
	group = new Group();
	pos_x = canvas_width - 20 - IMAGE_WIDTH;
	pos_y = 20 + IMAGE_HEIGHT;

	hit  = WindowUtil.getImage("dice/hit.png");
	miss = WindowUtil.getImage("dice/miss.png");


	hitL  = new Label("",new ImageView(hit));
	missL = new Label("",new ImageView(miss));

	hitL.setLayoutX(pos_x);
	hitL.setLayoutY(pos_y);
	missL.setLayoutX(pos_x);
	missL.setLayoutY(pos_y);

    }

    public void setHit(){
	group.getChildren().add(hitL);
	currentL = hitL;
	setAction();
    }

    public void setMiss(){
	group.getChildren().add(missL);
	currentL = missL;
	setAction();
    }

    public void setAction(){
	prevTime    = Calendar.getInstance().getTimeInMillis();
	currentTime = prevTime;
	counting    = true;
    }

    public Group getGroup(){
	return group;
    }

    public void reset(){
	if(currentL != null){
	    group.getChildren().remove(currentL);
	}
    }

    @Override
    public void update() {
	/*
	if(counting){
	    currentTime  = Calendar.getInstance().getTimeInMillis();
	    if(currentTime - prevTime > TIMEOUT){
		// remove
		group.getChildren().remove(currentL);
		counting = false;
	    }
	}
*/
    }
}
