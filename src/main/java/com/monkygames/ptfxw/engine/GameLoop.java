/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monkygames.ptfxw.engine;

import com.monkygames.ptfxw.game.SmallBase;
import com.monkygames.ptfxw.engine.hud.ManeuverSlot;
import com.monkygames.ptfxw.engine.hud.PointsSlot;
import com.monkygames.ptfxw.game.BaseZone;
import com.monkygames.ptfxw.game.ShipBase;
import com.monkygames.ptfxw.game.maneuvers.Maneuver;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;

/**
 *
 */
public class GameLoop extends AnimationTimer implements EventHandler<MouseEvent> {
    
    //private final long startNanoTime;

    private final GraphicsContext gc;

    private final int width;
    private final int height;

    // game components
    private Timer timer;
    private ManeuverSlot maneuverSlot;
    private PointsSlot pointsSlot;
    private Group shipBaseGroup;

    private ShipBase shipBase;

    private Group templateGroup;

    /**
     * The zone that is aiming for.
     */
    private BaseZone baseZone;
    private GameManager.ShipType shipType;
    private Maneuver maneuver;
    private GameManager gameManager;
    private Dice dice;
    private boolean isTracking = true;


    public GameLoop(GraphicsContext gc, int width, int height){
	this.gc = gc;
        //startNanoTime = System.nanoTime();
	this.width  = width;
	this.height = height;
    }

    @Override
    public void start(){
	super.start();
	if(timer != null){
	    timer.start();
	}
    }

    public void setTimer(Timer timer){
	this.timer = timer;
    }
    public void setManeuverSlot(ManeuverSlot maneuverSlot){
	this.maneuverSlot = maneuverSlot;
    }
    public void setPointsSlot(PointsSlot pointsSlot){
	this.pointsSlot = pointsSlot;
    }
    public void setShipBaseGroup(Group shipBaseGroup){
	this.shipBaseGroup = shipBaseGroup;
    }
    public void setBaseZone(BaseZone baseZone){
	this.baseZone = baseZone;
    }
    public void setTemplateGroup(Group templateGroup){
	this.templateGroup = templateGroup;
    }
    public void setShipType(GameManager.ShipType shipType){
	this.shipType = shipType;
    }
    public void setManeuver(Maneuver maneuver){
	this.maneuver = maneuver;
    }
    public void setGameManager(GameManager gameManager){
	this.gameManager = gameManager;
    }
    public void setDice(Dice dice){
	this.dice = dice;
    }
    
    public void handle(long currentNanoTime) {
	gc.clearRect(0,0,width,height);

	//double t = (currentNanoTime - startNanoTime) / 1000000000.0;
	if(timer != null){
	    //timer.render(gc);
	    timer.update();
	}
	if(maneuverSlot != null){
	    maneuverSlot.render(gc);
	}
	if(pointsSlot != null){
	    pointsSlot.render(gc);
	}
    }

    @Override
    public void handle(MouseEvent event) {
	double x = event.getSceneX();
	double y = event.getSceneY();

	if(isTracking){
	    isTracking = false;
	    Platform.runLater(new Runnable() {
		@Override
		public void run() {
		    // clear previous ship base
		    if(shipBase != null){
			shipBaseGroup.getChildren().remove(shipBase.getGroup());
		    }

		    // create a new shipbase
		    switch(shipType){
			case SMALL:
			    shipBase = new SmallBase(width,height);
			    break;
		    }
		    shipBase.setMidPosition(x,y);
		    shipBaseGroup.getChildren().add(shipBase.getGroup());

		    maneuver.rotateShip(shipBase);

		    // check if contains
		    Bounds baseZoneBounds = baseZone.getGroup().localToScene(baseZone.getGroup().getBoundsInLocal());
		    Bounds shipBaseBounds = shipBase.getGroup().localToScene(shipBase.getGroup().getBoundsInLocal());
		    if(baseZoneBounds.contains(shipBaseBounds)){
			gameManager.setResult(true);
		    }else{
			gameManager.setResult(false);
		    }
		}
	    });
	}
    }

    /**
     * Rests for the next player move.
     */
    public void reset(){
	isTracking = true;
	if(shipBase != null){
	    shipBaseGroup.getChildren().remove(shipBase.getGroup());
	}
    }
    public void stopTracking(){
	isTracking = false;
    }

}
