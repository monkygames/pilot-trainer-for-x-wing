package com.monkygames.ptfxw.engine.hud;

import com.monkygames.ptfxw.util.WindowUtil;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class PointsSlot extends HudCorner{

    /**
     * The speed label.
     */
    private final Label pointsL;
    private final Label iconL;

    /**
     * The total number of points.
     */

    private int points;

    
    public PointsSlot(int canvas_width, int canvas_height){
	super(canvas_width,canvas_height,HudCorner.TOP_RIGHT);

	pointsL = new Label("000");
	pointsL.setFont(Font.font("Times New Roman", FontWeight.BOLD, 30));
	pointsL.setLayoutX(canvas_width-70);
	pointsL.setLayoutY(8);

	iconL = new Label("d");
	Font xwingFont = WindowUtil.loadFont("xwing-miniatures.ttf", 30);
	iconL.setFont(xwingFont);
	iconL.setLayoutX(canvas_width-100);
	iconL.setLayoutY(2);

	group.getChildren().add(pointsL);
	group.getChildren().add(iconL);

    }

    public int getPoints() {
	return points;
    }

    public void incrementPoints() {
	points++;
	String pointsS;
	if(points < 10){
	    pointsS = "00"+points;
	}else if(points < 100){
	    pointsS = "0"+points;
	}else{
	    pointsS = ""+points;
	}
	pointsL.setText(pointsS);
    }
    public void resetPoints(){
	points = 0;
	pointsL.setText("000");
    }
}
