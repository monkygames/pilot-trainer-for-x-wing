package com.monkygames.ptfxw.engine.hud;

import javafx.scene.Group;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 */
public class HudCorner {

    private final int canvas_width;
    private final int canvas_height;

    private final int hud_width;
    private final int hud_height;
    public static final int TOP_LEFT  = 0;
    public static final int TOP_RIGHT = 1;
    public static final int BOT_LEFT  = 2;
    public static final int BOT_RIGHT = 3;
    double pos_x = 0;
    double pos_y = 0;

    protected Group group;

    public HudCorner(int canvas_width, int canvas_height, int position){
	this.canvas_width  = canvas_width;
	this.canvas_height = canvas_height;

	hud_width = canvas_width/4;
	hud_height = canvas_height/10;

	switch(position){
	    case TOP_LEFT:
		pos_x = 0;
		pos_y = 0;
		break;
	    case TOP_RIGHT:
		pos_x = canvas_width-hud_width;
		pos_y = 0;
		break;
	    case BOT_LEFT:
		pos_x = 0;
		pos_y = canvas_height-hud_height;
		break;
	    case BOT_RIGHT:
		pos_x = canvas_width-hud_width;
		pos_y = canvas_height-hud_height;
		break;
	    default:
		pos_x = 0;
		pos_y = 0;
	}
	group = new Group();
    }

    public void render(GraphicsContext gc) {
	gc.setFill(Color.LIGHTGRAY);
	gc.setStroke(Color.LIGHTGRAY);
	gc.strokeRect(pos_x, pos_y, hud_width, hud_height);
    }

    public Group getGroup(){
	return group;
    }
}
