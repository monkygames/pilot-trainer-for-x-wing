/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monkygames.ptfxw.engine.hud;

import com.monkygames.ptfxw.game.maneuvers.Bank;
import com.monkygames.ptfxw.game.maneuvers.ManeuverDirection;
import com.monkygames.ptfxw.game.maneuvers.ManeuverType;
import com.monkygames.ptfxw.util.WindowUtil;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * Render the next maneuver position in the top left corner.
 */
public class ManeuverSlot extends HudCorner{

    /**
     * The speed label.
     */
    private Label speedL;
    /**
     * The icon which uses a different font.
     */
    private Label iconL;
    
    public ManeuverSlot(int canvas_width, int canvas_height){
	super(canvas_width,canvas_height,HudCorner.TOP_LEFT);

	speedL = new Label("0");
	speedL.setFont(Font.font("Times New Roman", FontWeight.BOLD, 30));
	speedL.setLayoutX(40);
	speedL.setLayoutY(8);

	iconL = new Label("8");
	Font xwingFont = WindowUtil.loadFont("xwing-miniatures.ttf", 30);
	iconL.setFont(xwingFont);
	iconL.setLayoutX(60);
	iconL.setLayoutY(5);

	group.getChildren().add(speedL);
	group.getChildren().add(iconL);

    }


    public void setSpeed(int speed){
	speedL.setText(speed+"");
    }

    public void setManeuver(ManeuverType type){
	switch(type){
	    case STRAIGHT:
	        iconL.setText("8");
		break;
	}
    }
    public void setDirection(ManeuverDirection direction){
	switch(direction){
	    case LEFT:
		iconL.setText(Bank.BANK_LEFT);
		break;
	    case RIGHT:
		iconL.setText(Bank.BANK_RIGHT);
		break;
	    case STRAIGHT:
		iconL.setText("8");
		break;
	}

    }

}
