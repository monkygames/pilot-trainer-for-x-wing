package com.monkygames.ptfxw.engine.hud;

import com.monkygames.ptfxw.engine.GameManager;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 *
 */
public class HudBot {

    private Button quitB;
    private Button okB;
    private BorderPane borderPane;
    private static final int BUTTON_WIDTH = 105;
    private static final int BUTTON_HEIGHT= 25;

    private GameManager gameManager;

    public HudBot(int canvas_width, int canvas_height, GameManager gameManager){

	this.gameManager = gameManager;

	quitB = new Button("quit");
	okB   = new Button("ok");

	okB.setOnAction(new EventHandler<ActionEvent>() {
	    @Override
	    public void handle(ActionEvent e) {
		gameManager.okEventFired();
	    }
	});

	quitB.setOnAction(new EventHandler<ActionEvent>() {
	    @Override
	    public void handle(ActionEvent e) {
		gameManager.quitEventFired();
	    }
	});

	quitB.setPrefSize(BUTTON_WIDTH,BUTTON_HEIGHT);
	okB.setPrefSize(BUTTON_WIDTH,BUTTON_HEIGHT);


	// add a layout
	borderPane = new BorderPane();
	GridPane gridPane = new GridPane();
	GridPane.setConstraints(quitB, 0, 0); 
	GridPane.setConstraints(okB, 1, 0); 
	gridPane.getChildren().addAll(quitB,okB);
	gridPane.setAlignment(Pos.CENTER);

	GridPane.setHalignment(quitB, HPos.LEFT);
	GridPane.setHalignment(okB, HPos.RIGHT);

	GridPane.setHgrow(quitB, Priority.ALWAYS);
	GridPane.setHgrow(okB, Priority.ALWAYS);

	borderPane.setBottom(gridPane);
	borderPane.setPrefWidth(canvas_width);
	gridPane.setPrefWidth(canvas_width);

    }

    public BorderPane getBorderPane(){
	return borderPane;
    }

    public Button getQuitB(){
	return quitB;
    }
    public Button getOkB(){
	return okB;
    }

    public void disable(){
	okB.setDisable(true);
    }

    public void reset(){
	okB.setDisable(false);
    }
    
}
