/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monkygames.ptfxw.engine;

import java.util.Calendar;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 *
 */
public class Timer implements EngineEvent{

    private long prevTime;
    private long currentTime;
    private double calc;
    private int time;

    private final double pos_x;
    private static final double POS_Y = 10;

    private final Label timeL;

    private final Group group;
    private static final int MAX_TIME = 30;
    private final GameManager gameManager;


    public Timer(int canvas_width, int canvas_height, GameManager gameManager){
	this.gameManager = gameManager;
	pos_x = canvas_width/2.0 - 10;

	timeL = new Label("0");
	timeL.setFont(Font.font("Times New Roman", FontWeight.BOLD, 18));
	timeL.setLayoutX(pos_x);
	timeL.setLayoutY(POS_Y);

	group = new Group();
	group.getChildren().add(timeL);
	time = MAX_TIME;
    }

    public void start(){
	time        = MAX_TIME;
	prevTime    = Calendar.getInstance().getTimeInMillis();
	currentTime = prevTime;

	timeL.setText(""+time);
    }

    @Override
    public void update(){
	currentTime = Calendar.getInstance().getTimeInMillis();
	calc = currentTime - prevTime;
	calc = calc/1000.0;
	if (calc >= 1){
	    //time -= calc;
	    time--;
	    prevTime = currentTime;

	    if(time >= 0){
		// only allow the time to be positive.
	        timeL.setText(time+"");
	    }

	    // check if time is 0
	    if(time <= 0){
		// update game manager
		gameManager.timeExpiredEventFired();
	    }
	}
    }

    public boolean outOfTime(){
	if(time <= 0){
	    return true;
	}
	return false;
    }

    public Group getGroup(){
	return group;
    }
}
