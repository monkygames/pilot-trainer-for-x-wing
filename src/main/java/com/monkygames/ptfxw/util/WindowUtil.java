/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monkygames.ptfxw.util;

import com.monkygames.ptfxw.controller.FXMLController;
import java.io.IOException;
import java.net.URL;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 */
public class WindowUtil {
    public static final String fxml_offset = "/com/monkygames/ptfxw/fxml/";
    public static Application application;

    
    public static void setApplication(Application application){
	WindowUtil.application = application;
    }
    
    /**
     * Loads a fxml scene.
     * @param url offset from 'com/monkygames/ptfxw/fxml'
     * @return a controller object if success and null on failure.
     */
    public static FXMLController loadScene(String url){
	FXMLController controller = null;
	Stage stage = new Stage();
	try{
            URL location          = application.getClass().getResource(fxml_offset+url);

            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(location);
            fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());

            Parent root  = (Parent)fxmlLoader.load(location.openStream());
            controller   = fxmlLoader.getController();
	    Scene scene  = new Scene(root);

	    stage.setScene(scene);
	    controller.setStage(stage);
	}catch(IOException e){
	    return null;
	}
	return controller;
    }

    /**
     * Loads a font from the fonts directory.
     * @param url
     * @param size
     * @return 
     */
    public static Font loadFont(String url,int size){
	Font font = null;
	font = Font.loadFont(application.getClass().getResourceAsStream("/fonts/"+url), size);
	return font;
    }

    public static Image getImage(String url){
	Image image = null;
	image = new Image(application.getClass().getResourceAsStream("/images/"+url));
	return image;
    }
}